﻿using System;

namespace MyFirstCSharpApp
{
    class Program
    {
        static void Main(string[] args)
        {
          
            LargestProduct product = new LargestProduct("Resources//euler11.txt");
             int[,] myGrid = product.reading();
            Console.WriteLine(product.maximum(myGrid));
        }
    }
}
