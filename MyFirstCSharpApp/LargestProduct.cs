using System;

namespace MyFirstCSharpApp
{
    class LargestProduct
    {
        public string path;
        public LargestProduct(string Path)
        {
            this.path=Path;
        }

        public int[,] reading()
        {
            
            int[,] myGrid = new int[20,20];
            string[] nums = System.IO.File.ReadAllLines(this.path);
            string[] oldnums = new string[20];
            
            for (int i = 0; i<nums.Length; i++)
            {
                oldnums = nums[i].Split(" ");
                for (int j=0; j<nums.Length; j++){
                    myGrid[i,j]=int.Parse(oldnums[j]);
                }
                
            }
           /* for (int i = 0; i<nums.Length; i++)
            {
             Console.WriteLine(myGrid[i,0]);
            }
            
             Console.WriteLine(nums[0]);
             oldnums = nums[0].Split(" ");
             Console.WriteLine(oldnums[0]);
             int[] newnums = new int[2];
             newnums[0] = Int32.Parse(oldnums[0]);
             Console.WriteLine(newnums[0]);*/
            /* int[] newnum = Int32.Parse(nums[0].Split(" "));
             myGrid[0,0]=newnum[0];
             Console.WriteLine(myGrid[0,0]);*/
            return myGrid;
        }

        public long maximum( int[,] myGrid)
        {
            return myGrid[1,1];
        }
    }
}
